from torch.distributions import Normal
from torch.distributions import Categorical
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from single_integrator import SingleIntegrator
from single_integrator_cont import SingleIntegratorCont
import math

gamma = 0.99
total_episode = 5000
DISCRETE = 0
CONTINUOUS = 1
action_type = CONTINUOUS
ENV_GYM = 0

class Pi(nn.Module):
    def __init__(self, in_dim, out_dim, type_act, max_action):
        super(Pi, self).__init__()
        self.act_type = type_act
        if self.act_type == CONTINUOUS:
            self.max_action = max_action
        if self.act_type == DISCRETE:
            layers = [
                nn.Linear(in_dim, 64),
                nn.ReLU(),
                nn.Linear(64, out_dim),
                ]
            self.model = nn.Sequential(*layers)
        else:
            self.base = nn.Sequential(
                        nn.Linear(in_dim, 64),
                        nn.ReLU(),
                        )
            self.mu = nn.Sequential(
                        nn.Linear(64, out_dim),
                        nn.Tanh(),
                        )
            self.var = nn.Sequential(
                        nn.Linear(64, out_dim),
                        nn.Softplus(),
                        )
        self.onpolicy_reset()
        self.train() # set training mode

    def onpolicy_reset(self):
        self.log_probs = []
        self.rewards = []

    def forward(self, x):
        if self.act_type == DISCRETE:
            pdparam = self.model(x)
            return pdparam
        else:
            base_out = self.base(x)
            # return self.mu(base_out), self.var(base_out)
            return self.mu(base_out)
    
    def act(self, state):
        x = torch.from_numpy(state.astype(np.float32)) # to tensor
        if ENV_GYM==0:
            x = x.unsqueeze(dim=0)
        if self.act_type == DISCRETE:
            pdparam = self.forward(x) # forward pass
            pd = Categorical(logits=pdparam) # probability distribution
            # print("Cat")
            # print("pd")
            # print(pd)
            action = pd.sample() # pi(a|s) in action via pd
        else:
            # param_mu, param_var = self.forward(x)
            # param_std = torch.sqrt(param_var)
            # pd = Normal(loc=param_mu, scale=param_var)
            param_mu = self.forward(x)
            param_var = 0.1
            pd = Normal(loc=param_mu, scale=param_var)
            # print("NORM")
            # print(param_mu)
            # print(param_var)
            action = pd.sample() # pi(a|s) in action via pd
            action = np.clip(action, -self.max_action, self.max_action)
            
        # print(action)
        log_prob = pd.log_prob(action) # log_prob of pi(a|s)
        # print(log_prob)
        self.log_probs.append(log_prob) # store for training
        if self.act_type == DISCRETE:
            return action.item()
        else:
            return [action.item()]

def train(pi, optimizer):
    # Inner gradient-ascent loop of REINFORCE algorithm
    T = len(pi.rewards)
    rets = np.empty(T, dtype=np.float32) # the returns
    future_ret = 0.0
    # compute the returns efficiently
    for t in reversed(range(T)):
        future_ret = pi.rewards[t] + gamma * future_ret
        rets[t] = future_ret
    rets = torch.tensor(rets)
    # print(rets)
    # print("train")
    # print(pi.log_probs)
    log_probs = torch.stack(pi.log_probs)
    # print(log_probs)
    loss = - log_probs * rets # gradient term; Negative for maximizing
    # print(loss)
    loss = torch.sum(loss)
    optimizer.zero_grad()
    loss.backward() # backpropagate, compute gradients
    optimizer.step() # gradient-ascent, update the weights
    return loss

if __name__ == '__main__':
    if action_type == DISCRETE:
        if ENV_GYM:
            env = gym.make('CartPole-v0')
            out_dim = env.action_space.n # 2
            in_dim = env.observation_space.shape[0] # 4
            REWARD_BOUNDARY = 195
        else:
            env = SingleIntegrator()
            out_dim = env.action_space # 2
            in_dim = env.observation_space # 1
            REWARD_BOUNDARY = 490
    else:
        if ENV_GYM:
            env = gym.make('Pendulum-v0')
            out_dim = 2*env.action_space.shape[0]# 2 is no of params of normal distrib
            in_dim = env.observation_space.shape[0] # 4
        else:
            env = SingleIntegratorCont()
            out_dim = env.action_space # 1
            in_dim = env.observation_space # 1
            REWARD_BOUNDARY = 490
    
    if not ENV_GYM:
        if action_type == DISCRETE:
            pi = Pi(in_dim, out_dim, action_type, _) # policy pi_theta for REINFORCE
        else:
            pi = Pi(in_dim, out_dim, action_type, env.max_speed) # policy pi_theta for REINFORCE
    optimizer = optim.Adam(pi.parameters(), lr=0.01)
    final_reward = []
    final_loss = []
    for epi in range(total_episode):
        state = env.reset()
        for t in range(200): # cartpole max timestep is 200
            action = pi.act(state)
            # print("tes")
            # print(state)
            # print(action)
            state, reward, done, _ = env.step(action)
            # print(state)
            # print(reward)
            # print(done)
            pi.rewards.append(reward)
            if ENV_GYM:
                env.render()
            if done:
                break
        loss = train(pi, optimizer) # train per episode
        final_loss.append(loss.detach())
        total_reward = sum(pi.rewards)
        final_reward.append(total_reward)
        solved = total_reward > REWARD_BOUNDARY
        pi.onpolicy_reset() # onpolicy: clear memory after training
        print(f'Episode {epi}, loss: {loss}, \
        total_reward: {total_reward}, solved: {solved}')
        if math.isnan(state):
            break
    fig, ax = plt.subplots()
    ax.plot(final_reward, '-r')
    plt.ylabel('Reward',fontsize=24)
    if action_type == CONTINUOUS:
        plt.ylim(-50, 10)
    # plt.xlim(0,10)
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(final_loss, '-r')
    plt.ylabel('Loss',fontsize=24)
    plt.show()