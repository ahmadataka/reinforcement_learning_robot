from torch.distributions import Categorical
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from single_integrator import SingleIntegrator
from single_integrator_cont import SingleIntegratorCont
import random

ENV_GYM = 1
gamma = 0.99
total_iter = 10#250000
EPSILON_START = 1.0
EPSILON_STOP = 0.02
EPSILON_STEPS = 5000
BATCH_SIZE = 8
REPLAY_BUFFER = 50000
training_num = 1
batch_num = 1
class Pi(nn.Module):
    def __init__(self, in_dim, out_dim, env):
        super(Pi, self).__init__()
        layers = [
            nn.Linear(in_dim, 128),
            nn.ReLU(),
            nn.Linear(128, out_dim),
            ]
        self.model = nn.Sequential(*layers)
        self.train() # set training mode
        self.env = env

    def forward(self, x):
        Q_val = self.model(x)
        return Q_val
    
    def act(self, state, eps):
        if np.random.random() < eps:
            if ENV_GYM:
                action = self.env.action_space.sample()
            else:
                action = np.random.randint(self.env.action_space)
        else:
            x = torch.from_numpy(state.astype(np.float32)) # to tensor
            q_vals_v = self.forward(x) # forward pass
            act_v = torch.argmax(q_vals_v)
            action = act_v.item()
        return action

def train(pi, experience, optimizer, obs_space):
    states = []
    actions = []
    rewards = []
    new_states = []
    dones = []
    for i in range(0, len(experience)):
        states.append(experience[i][0])
        actions.append(experience[i][1])
        rewards.append(experience[i][2])
        new_states.append(experience[i][3])
        dones.append(experience[i][4])

    # print(states)
    # print(torch.FloatTensor(states))
    states_v = torch.FloatTensor(states).reshape(len(experience), obs_space)
    actions_v = torch.tensor(actions).reshape(len(experience), 1)
    rewards_v = torch.tensor(rewards).reshape(len(experience), 1)
    next_states_v = torch.FloatTensor(new_states).reshape(len(experience), obs_space)
    done_v = torch.BoolTensor(dones)

    # print("states")
    # print(states_v)
    action_val = pi.forward(states_v)
    # print(action_val)
    # print(actions_v)
    state_action_values =  action_val.gather(1,actions_v)
    # print(state_action_values)
    state_action_values = state_action_values.reshape(len(state_action_values))

    # print("next_states")
    # print(next_states_v)
    action_val = pi.forward(next_states_v)
    # print(action_val)
    # print(next_actions_v)
    next_state_values = action_val.max(1)[0]
    # print(next_state_values)
    next_state_values[done_v] = 0.0
    next_state_values = next_state_values.reshape(len(next_state_values),1)
    next_state_values = next_state_values.detach()
    # print(next_state_values)
    # print(rewards_v)
    expected_state_action_values = next_state_values * gamma + rewards_v
    # print(expected_state_action_values)
    loss = nn.MSELoss()(state_action_values, expected_state_action_values)
    # print(loss)
    optimizer.zero_grad()
    loss.backward() # backpropagate, compute gradients
    optimizer.step() # gradient-ascent, update the weights
    return loss

if __name__ == '__main__':
    if ENV_GYM:
        env = gym.make('CartPole-v0')
        in_dim = env.observation_space.shape[0] # 4
        out_dim = env.action_space.n # 2
        REWARD_BOUNDARY = 195
    else:
        env = SingleIntegrator()
        out_dim = env.action_space # 2
        in_dim = env.observation_space # 1
        REWARD_BOUNDARY = 490
    pi = Pi(in_dim, out_dim, env) # policy pi_theta for REINFORCE
    optimizer = optim.Adam(pi.parameters(), lr=0.01)
    epsilon = EPSILON_START
    state = env.reset()
    action = pi.act(state, epsilon)
    single_batch = [state, action, 0.0, state, False]
    experience = [single_batch]*REPLAY_BUFFER
    final_reward = []
    final_loss = []
    total_reward = 0
    most_update_total_reward = 0
    for itera in range(total_iter):    
        print("iter:")
        print(itera)
        action = pi.act(state, epsilon)
        new_state, reward, done, _ = env.step(action)
        new_data = [state, action, reward, new_state, done]
        print(new_data)
        experience[itera%REPLAY_BUFFER] = new_data
        # print(experience)
        total_reward += reward
        if ENV_GYM:
            env.render()
        state = new_state
        if done:
            final_reward.append(total_reward)
            most_update_total_reward = total_reward
            total_reward = 0
            state = env.reset()
            
        if (itera<BATCH_SIZE):
            # Sample a batch from experience buffer
            continue
        
        for batch_iter in range(0, batch_num):
            if(itera>=REPLAY_BUFFER):
                sampled_exp = experience
            else:
                sampled_exp = experience[0:itera]
            # print("sampled")
            print(sampled_exp)
            exp = random.sample(sampled_exp, BATCH_SIZE)
            print(exp)
            for training_iter in range(0, training_num):
                # print(batch_iter)
                # print(training_iter)
                loss = train(pi, exp, optimizer, in_dim) # train per episode
        if done:
            final_loss.append(loss)
            solved = most_update_total_reward > REWARD_BOUNDARY
            print(f'Iteration {itera}, loss: {loss}, \
            total_reward: {most_update_total_reward}, solved: {solved}, eps: {epsilon}')
        epsilon = max(EPSILON_STOP, EPSILON_START - itera / EPSILON_STEPS)

    fig, ax = plt.subplots()
    ax.plot(final_reward, '-r')
    plt.ylabel('Reward',fontsize=24)
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(final_loss, '-r')
    plt.ylabel('Loss',fontsize=24)
    plt.show()
