from torch.distributions import Categorical
from torch.distributions import Normal
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from single_integrator import SingleIntegrator
from single_integrator_cont import SingleIntegratorCont
import math

gamma = 0.99
total_episode = 4000
DISCRETE = 0
CONTINUOUS = 1
action_type = DISCRETE
ENV_GYM = 1

class Actor(nn.Module):
    def __init__(self, in_dim, out_dim, type_act, max_action):
        super(Actor, self).__init__()
        self.act_type = type_act
        if self.act_type == CONTINUOUS:
            self.max_action = max_action
        if self.act_type == DISCRETE:
            layers = [
                nn.Linear(in_dim, 64),
                nn.ReLU(),
                nn.Linear(64, out_dim),
                ]
            self.model = nn.Sequential(*layers)
        else:
            self.base = nn.Sequential(
                        nn.Linear(in_dim, 64),
                        nn.ReLU(),
                        )
            self.mu = nn.Sequential(
                        nn.Linear(64, out_dim),
                        nn.Tanh(),
                        )
            self.var = nn.Sequential(
                        nn.Linear(64, out_dim),
                        nn.Softplus(),
                        )
        
        self.onpolicy_reset()
        self.train() # set training mode

    def onpolicy_reset(self):
        self.log_probs = []

    def forward(self, x):
        if self.act_type == DISCRETE:
            pdparam = self.model(x)
            return pdparam
        else:
            base_out = self.base(x)
            return self.mu(base_out), self.var(base_out)
            # return self.mu(base_out)
    
    def act(self, state):
        x = torch.from_numpy(state.astype(np.float32)) # to tensor
        if ENV_GYM==0:
            x = x.unsqueeze(dim=0)
        if self.act_type == DISCRETE:
            pdparam = self.forward(x) # forward pass
            pd = Categorical(logits=pdparam) # probability distribution
            # print("Cat")
            # print("pd")
            # print(pd)
            action = pd.sample() # pi(a|s) in action via pd
        else:
            param_mu, param_var = self.forward(x)
            pd = Normal(loc=param_mu, scale=param_var)
            print("NORM")
            print(param_mu)
            print(param_var)
            action = pd.sample() # pi(a|s) in action via pd
            action = np.clip(action, -self.max_action, self.max_action)
        # print(action)
        log_prob = pd.log_prob(action) # log_prob of pi(a|s)
        # print(log_prob)
        self.log_probs.append(log_prob) # store for training
        if self.act_type == DISCRETE:
            return action.item()
        else:
            return [action.item()]

class Critic(nn.Module):
    def __init__(self, in_dim, out_dim):
        super(Critic, self).__init__()
        layers = [
            nn.Linear(in_dim, 64),
            nn.ReLU(),
            nn.Linear(64, out_dim),
            ]
        self.model = nn.Sequential(*layers)
        self.train() # set training mode

    def forward(self, x):
        value = self.model(x)
        return value

def train(pi, values, optimizer_act, optimizer_critic, experience, obs_space):
    # Inner gradient-ascent loop of REINFORCE algorithm
    states = []
    actions = []
    rewards = []
    new_states = []
    dones = []
    for i in range(0, len(experience)):
        states.append(experience[i][0])
        actions.append(experience[i][1])
        rewards.append(experience[i][2])
        new_states.append(experience[i][3])
        dones.append(experience[i][4])
    # print(states)
    # print(experience)
    # print(len(experience))
    # print(obs_space)
    states_v = torch.FloatTensor(states).reshape(len(experience), obs_space)
    actions_v = torch.tensor(actions).reshape(len(experience), 1)
    rewards_v = torch.tensor(rewards).reshape(len(experience), 1)
    next_states_v = torch.FloatTensor(new_states).reshape(len(experience), obs_space)
    done_v = torch.ByteTensor(dones)

    # print("states")
    # print(states_v)
    state_values = values.forward(states_v)
    # print(action_val)
    # print(actions_v)
    state_values = state_values.reshape(len(state_values))
    # print(state_values)

    # print("next_states")
    # print(next_states_v)
    next_state_values = values.forward(next_states_v)
    # print(action_val)
    # print(next_actions_v)
    # print(next_state_values)
    next_state_values[done_v] = 0.0
    next_state_values = next_state_values.detach()# Gradient will not be taken into account
    # print(next_state_values)
    # print(rewards_v)
    expected_state_values = next_state_values * gamma + rewards_v
    expected_state_values = expected_state_values.reshape(len(expected_state_values))
    # print(expected_state_values)
    advantage = expected_state_values - state_values
    loss_v = nn.MSELoss()(state_values, expected_state_values)
    optimizer_critic.zero_grad()
    loss_v.backward() # backpropagate, compute gradients
    optimizer_critic.step() # gradient-ascent, update the weights
    # print("A")
    # print(advantage)

    log_probs = torch.stack(pi.log_probs)
    # print(log_probs)
    loss = - log_probs * advantage.detach() # gradient term; Negative for maximizing. # Gradient of advantage will not be taken into account
    # print(loss)
    loss = torch.sum(loss)
    optimizer_act.zero_grad()
    loss.backward() # backpropagate, compute gradients
    optimizer_act.step() # gradient-ascent, update the weights
    return loss_v, loss 

if __name__ == '__main__':
    if action_type == DISCRETE:
        if ENV_GYM:
            env = gym.make('CartPole-v0')
            out_dim = env.action_space.n # 2
            in_dim = env.observation_space.shape[0] # 4
            REWARD_BOUNDARY = 195
        else:
            env = SingleIntegrator()
            out_dim = env.action_space # 2
            in_dim = env.observation_space # 1
            REWARD_BOUNDARY = 490
    else:
        if ENV_GYM:
            env = gym.make('Pendulum-v0')
            out_dim = 2*env.action_space.shape[0]# 2 is no of params of normal distrib
            in_dim = env.observation_space.shape[0] # 4
        else:
            env = SingleIntegratorCont()
            out_dim = env.action_space # 1
            in_dim = env.observation_space # 1
            REWARD_BOUNDARY = 490

    
    if action_type == DISCRETE:
        pi = Actor(in_dim, out_dim, action_type, _) # policy pi_theta for REINFORCE
    else:
        pi = Actor(in_dim, out_dim, action_type, env.max_speed) # policy pi_theta for REINFORCE

    values = Critic(in_dim, 1) # critic for value prediction

    optimizer_act = optim.Adam(pi.parameters(), lr=0.01)
    optimizer_critic = optim.Adam(values.parameters(), lr=0.01)
    final_reward = []
    final_loss_pi = []
    final_loss_v = []
    experience = []
    for epi in range(total_episode):
        state = env.reset()
        total_reward = 0
        for t in range(200): # cartpole max timestep is 200
            action = pi.act(state)
            new_state, reward, done, _ = env.step(action)
            new_data = [state, action, reward, new_state, done]
            print(new_data)
            experience.append(new_data)
            state = new_state
            total_reward += reward
            if ENV_GYM:
                env.render()
            if done:
                break
        loss_v, loss_pi = train(pi, values, optimizer_act, optimizer_critic, experience, in_dim) # train per episode
        final_loss_v.append(loss_v)
        final_loss_pi.append(loss_pi)
        final_reward.append(total_reward)
        solved = total_reward > REWARD_BOUNDARY
        pi.onpolicy_reset() # onpolicy: clear memory after training
        experience = [] # reset experience - online policy
        print(f'Episode {epi}, loss_v: {loss_v}, loss_pi: {loss_pi},\
        total_reward: {total_reward}, solved: {solved}')
        if not ENV_GYM:
            if math.isnan(state):
                break
    fig, ax = plt.subplots()
    ax.plot(final_reward, '-r')
    plt.ylabel('Reward',fontsize=24)
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(final_loss_v, '-r')
    plt.ylabel('Loss',fontsize=24)
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(final_loss_pi, '-r')
    plt.ylabel('Loss',fontsize=24)
    plt.show()