import math
import numpy as np
import random

DISCRETE = 0
CONTINUOUS = 1
class SingleIntegrator():
    def __init__(self):
        self.MAX_EPISODE = 100
        self.x_threshold = 5
        
        high = np.array([self.x_threshold],
                        dtype=np.float32)

        self.action_space = 3
        self.observation_space = 1
        self.steps_left = self.MAX_EPISODE
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.x_target = 0

    def step(self, action):
        x = self.state
        save_state = self.state
        if action == 1:
            x = x+1
        elif action == 2:
            x = x-1
        else:
            x = x
        self.state = x
        done = bool(
            x < -self.x_threshold
            or x > self.x_threshold
            or self.steps_left<0
        )

        # if x == 0:
        #     reward = 5
        # else:
        #     reward = 0
        if not done:
            reward = - x**2
        else:
            if x< - self.x_threshold or x > self.x_threshold:
                reward = -x**2*self.MAX_EPISODE
            else:
                reward = 0
        if not done:
            self.steps_left = self.steps_left-1
        
        return np.array([self.state]), reward, done, {}

    def reset(self):
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.steps_left = self.MAX_EPISODE
        return np.array([self.state])