import math
import numpy as np
import random

class SingleIntegratorCont():
    def __init__(self):
        self.MAX_EPISODE = 10
        self.x_threshold = 5
        self.delta_t = 1
        high = np.array([self.x_threshold],
                        dtype=np.float32)

        self.action_space = 1
        self.observation_space = 1
        self.steps_left = self.MAX_EPISODE
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.x_target = 0
        self.max_speed = 1

    def step(self, action):
        x = self.state
        save_state = self.state
        action = np.clip(action, -self.max_speed, self.max_speed)[0]
        x = x + action*self.delta_t
        self.state = x
        done = bool(
            x < -self.x_threshold
            or x > self.x_threshold
            or self.steps_left<0
        )
        if not done:
            reward = - x**2 - action**2
            # reward = - x**2
        else:
            if x< - self.x_threshold or x > self.x_threshold:
                reward = -x**2*self.MAX_EPISODE
            else:
                reward = 0
        # if reward< -100:
        #     reward = -100
        if not done:
            self.steps_left = self.steps_left-1
        return np.array([self.state]), reward, done, {}

    def reset(self):
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.steps_left = self.MAX_EPISODE
        return np.array([self.state])