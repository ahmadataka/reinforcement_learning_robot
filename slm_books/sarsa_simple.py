from torch.distributions import Categorical
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

gamma = 0.99
total_episode = 10000
EPSILON_START = 1.0
EPSILON_STOP = 0.02
EPSILON_STEPS = 0.5*total_episode

class Pi(nn.Module):
    def __init__(self, in_dim, out_dim, env):
        super(Pi, self).__init__()
        layers = [
            nn.Linear(in_dim, 64),
            nn.ReLU(),
            nn.Linear(64, out_dim),
            ]
        self.model = nn.Sequential(*layers)
        self.train() # set training mode
        self.env = env

    def forward(self, x):
        Q_val = self.model(x)
        return Q_val
    
    def act(self, state, eps):
        if np.random.random() < eps:
            action = self.env.action_space.sample()
        else:
            x = torch.from_numpy(state.astype(np.float32)) # to tensor
            q_vals_v = self.forward(x) # forward pass
            act_v = torch.argmax(q_vals_v)
            action = act_v.item()
        return action

def train(pi, experience, optimizer, obs_space):
    states = []
    actions = []
    rewards = []
    new_states = []
    new_actions = []
    dones = []
    for i in range(0, len(experience)):
        states.append(experience[i][0])
        actions.append(experience[i][1])
        rewards.append(experience[i][2])
        new_states.append(experience[i][3])
        new_actions.append(experience[i][4])
        dones.append(experience[i][5])

    states_v = torch.FloatTensor(states).reshape(len(experience), obs_space)
    actions_v = torch.tensor(actions).reshape(len(experience), 1)
    rewards_v = torch.tensor(rewards).reshape(len(experience), 1)
    next_states_v = torch.FloatTensor(new_states).reshape(len(experience), obs_space)
    next_actions_v = torch.tensor(new_actions).reshape(len(experience), 1)
    done_v = torch.ByteTensor(dones)

    # print("states")
    # print(states_v)
    action_val = pi.forward(states_v)
    # print(action_val)
    # print(actions_v)
    state_action_values =  action_val.gather(1,actions_v)
    # print(state_action_values)
    state_action_values = state_action_values.reshape(len(state_action_values))

    # print("next_states")
    # print(next_states_v)
    action_val = pi.forward(next_states_v)
    # print(action_val)
    # print(next_actions_v)
    next_state_values = action_val.gather(1,next_actions_v)
    # print(next_state_values)
    next_state_values[done_v] = 0.0
    next_state_values = next_state_values.detach()
    # print(next_state_values)
    # print(rewards_v)
    expected_state_action_values = next_state_values * gamma + rewards_v
    # print(expected_state_action_values)
    loss = nn.MSELoss()(state_action_values, expected_state_action_values)
    optimizer.zero_grad()
    loss.backward() # backpropagate, compute gradients
    optimizer.step() # gradient-ascent, update the weights
    return loss

if __name__ == '__main__':
    env = gym.make('CartPole-v0')
    in_dim = env.observation_space.shape[0] # 4
    out_dim = env.action_space.n # 2
    pi = Pi(in_dim, out_dim, env) # policy pi_theta for REINFORCE
    optimizer = optim.Adam(pi.parameters(), lr=0.01)
    experience = []
    final_reward = []
    final_loss = []
    epsilon = EPSILON_START
    for epi in range(total_episode):
        state = env.reset()
        total_reward = 0
        for t in range(200): # cartpole max timestep is 200
            action = pi.act(state, epsilon)
            new_state, reward, done, _ = env.step(action)
            new_action = pi.act(new_state, epsilon)
            new_data = [state, action, reward, new_state, new_action, done]
            experience.append(new_data)
            total_reward += reward
            env.render()
            state = new_state
            if done:
                break
        # print("exp")
        # print(experience)
        loss = train(pi, experience, optimizer, in_dim) # train per episode
        final_loss.append(loss)
        final_reward.append(total_reward)
        solved = total_reward > 195.0
        print(f'Episode {epi}, loss: {loss}, \
        total_reward: {total_reward}, solved: {solved}, eps: {epsilon}')
        epsilon = max(EPSILON_STOP, EPSILON_START - epi / EPSILON_STEPS)
        experience = [] # reset experience - online policy

    fig, ax = plt.subplots()
    ax.plot(final_reward, '-r')
    plt.ylabel('Reward',fontsize=24)
    plt.show()

    fig, ax = plt.subplots()
    ax.plot(final_loss, '-r')
    plt.ylabel('Loss',fontsize=24)
    plt.show()
