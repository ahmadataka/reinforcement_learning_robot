import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
# import matplotlib.pyplot as plt

a = torch.FloatTensor(3, 2)
a.zero_()
print(a)
print(torch.FloatTensor([[1,2,3],[3,2,1]]))
n = np.zeros(shape=(3, 2), dtype=np.float32)
print(torch.tensor(n))

s = nn.Sequential(nn.Linear(2, 5),
    nn.ReLU(),
    nn.Linear(5, 20),
    nn.ReLU(),
    nn.Linear(20, 10),
    # nn.Dropout(p=0.3),
    nn.Softmax(dim=1)
    )
print(s)
print(s(torch.FloatTensor([[5,2],[2,1]])))
testing = s(torch.FloatTensor([[1,2]])).max(1)[0]
testing2 = s(torch.FloatTensor([[1,2]]))[0,3]
print(testing)
print(testing2)
inde = torch.ByteTensor([1])
print(inde)
testing[inde] = 0.0
print(testing)

class DQN(nn.Module):
    def __init__(self, input_shape, n_actions):
        super(DQN, self).__init__()
        hidden = 8
        self.fc = nn.Sequential(
            nn.Linear(input_shape, hidden),
            nn.ReLU(),
            nn.Linear(hidden, n_actions),
            # nn.Softmax(dim=0)
        )

    def forward(self, x):
        return self.fc(x)

test_net = DQN(1, 1)
tgt_net = DQN(1, 1)
objective = nn.MSELoss()
optimizer = optim.Adam(params=test_net.parameters(), lr=0.01)
for iter in range(0,10):
    for ind in range(0, 100):
        i = np.linspace(0.1*ind,0.1*(ind+1),10)
        # y = 2*i + 3
        y = np.ones(i.shape)
        input_data = torch.FloatTensor([i]).reshape(len(i), 1)
        out_real = torch.FloatTensor([y]).reshape(len(i), 1)
        out_test = test_net(input_data)
        loss_v = objective(out_test, out_real)
        optimizer.zero_grad()
        loss_v.backward()
        optimizer.step()
        print(loss_v)
i = np.linspace(0,10,100)
# print(i.shape)
# y = 2*i + 3
y = np.ones(i.shape)
tgt_net.load_state_dict(test_net.state_dict())
input_full = torch.FloatTensor([i]).reshape(len(i), 1)
out_full = torch.FloatTensor([y]).reshape(len(i), 1)
out_final = tgt_net(input_full)
out_ori = test_net(input_full)
# print(out_full-out_final)
# print(out_full-out_ori)
# print(loss_v)
print(out_final)
print(out_full)
# fig, ax = plt.subplots()
# ax.plot(out_full)
# ax.plot(out_final)
# plt.show()

