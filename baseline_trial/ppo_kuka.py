import os
import gym
import numpy as np
import matplotlib.pyplot as plt
import pybullet_envs.bullet.kukaGymEnv as e

from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy

env = e.KukaGymEnv(renders=False)
print(env.reset())
print(env.observation_space)

model = PPO('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=3000000)
model.save("ppo_kuka")
del model  # delete trained model to demonstrate loading
env.close()
del env
# Load the trained agent
env = e.KukaGymEnv(renders=True)
model = PPO.load("ppo_kuka", env=env)
mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

obs = env.reset()
for i in range(1000):
    action, _state = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    # print(i)
    if done:
      obs = env.reset()