import gym
import numpy as np
import pybulletgym  # register PyBullet enviroments with open ai gym
import matplotlib.pyplot as plt

from stable_baselines3 import SAC
from time import sleep

ENV_ID = "ReacherPyBulletEnv-v0"
env = gym.make(ENV_ID)
env.render()

model = SAC('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=200000)
model.save("sac_reacher")
print("model saved, learning completed")

del model # remove to demonstrate saving and loading

model = SAC.load("sac_reacher")

obs = env.reset()
i = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    sleep(0.01)
    # print(i)
    i += 1
    if dones:
        i = 0
        env.reset()
        # print("dones")