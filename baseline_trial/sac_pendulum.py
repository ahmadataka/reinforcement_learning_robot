import gym
from stable_baselines3 import SAC

env = gym.make('Pendulum-v0')

model = SAC('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=200000)
model.save("sac_pendulum")
del model  # delete trained model to demonstrate loading

# Load the trained agent
model = SAC.load("sac_pendulum", env=env)

# mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

obs = env.reset()
for i in range(1000):
    action, _state = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    # print(i)
    if done:
      obs = env.reset()