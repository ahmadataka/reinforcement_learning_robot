import gym
import numpy as np
import matplotlib.pyplot as plt

from stable_baselines3 import A2C

from time import sleep

from stable_baselines3.common.env_checker import check_env

from planar_2_link_arm_IK import Planar2LinkArmIK

env = Planar2LinkArmIK()
# It will check your custom environment and output additional warnings if needed
check_env(env)

model = A2C('MlpPolicy', env, verbose=1, device="cpu")
model.learn(total_timesteps=200000, log_interval=10)
# model.save("a2c_planararmIK")
# env = model.get_env()
# print("learning finished, model saved")
# del model # remove to demonstrate saving and loading

# model = A2C.load("a2c_planararmIK", device="cpu")

obs = env.reset()
i = 0
total_reward = 0
error_x = []
error_y = []
speed_x = []
speed_y = []
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    total_reward = total_reward + rewards
    env.render()
    error_x.append(obs[0])
    speed_x.append(obs[1])
    error_y.append(obs[2])
    speed_y.append(obs[3])
    # sleep(0.01)
    # print(i)
    i += 1
    if dones:
        i = 0
        env.reset()
        # print(total_reward)
        total_reward = 0
        break
        # print("dones")

fig, ax = plt.subplots()
ax.plot(error_x, '-r')
ax.plot(error_y, '-g')
ax.plot(speed_x, '-b')
ax.plot(speed_y, '-k')
# plt.ylabel('Loss',fontsize=24)
plt.show()
