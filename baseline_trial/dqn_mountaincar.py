import gym
import numpy as np
import matplotlib.pyplot as plt
from stable_baselines3 import DQN
from stable_baselines3.common.evaluation import evaluate_policy


# Create environment
env = gym.make('MountainCar-v0')

# Instantiate the agent
model = DQN('MlpPolicy', env, verbose=1)

# # Load the agent
# model = DQN.load("dqn_mountaincar", env=env)

# Train the agent
model.learn(total_timesteps=int(10000000), log_interval=4)
# Save the agent
model.save("dqn_mountaincar_8M")
del model  # delete trained model to demonstrate loading

# Load the trained agent
model = DQN.load("dqn_mountaincar_8M")
# model.learn(total_timesteps=int(1))
# Evaluate the agent
# NOTE: If you use wrappers with your environment that modify rewards,
#       this will be reflected here. To evaluate with original rewards,
#       wrap environment in a "Monitor" wrapper before other wrappers.
# mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

# Enjoy trained agent
obs = env.reset()
dones_counting = 0
total_reward = 0
error_x = []
error_y = []
speed_x = []
speed_y = []
counting = 0
while True:
    # print(obs)
    action, _states = model.predict(obs, deterministic=True)
    # print(action)
    obs, rewards, dones, info = env.step(action)
    env.render()
    error_x.append(obs.item(0))
    error_y.append(obs.item(1))
    total_reward += rewards
    counting += 1
    if dones:
        obs = env.reset()
        print(total_reward)
        total_reward = 0
        dones_counting += 1
        if dones_counting==10:
            break
env.close()
fig, ax = plt.subplots()
ax.plot(error_x, '-r', label ='Error x')
ax.plot(error_y, '-g', label ='Error y')
plt.xlabel('Step',fontsize=12)
plt.legend()
# plt.xlim(0, 100)
plt.show()

