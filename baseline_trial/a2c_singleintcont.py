import gym
import numpy as np

from stable_baselines3 import A2C

from stable_baselines3.common.env_checker import check_env

from SingleIntegratorCont import SingleIntegratorCont

env = SingleIntegratorCont()
# It will check your custom environment and output additional warnings if needed
check_env(env)

model = A2C('MlpPolicy', env, verbose=1, device="cpu")
model.learn(total_timesteps=20000)
model.save("a2c_singleintcont")

del model # remove to demonstrate saving and loading

model = A2C.load("a2c_singleintcont", env=env, device="cpu")

obs = env.reset()
dones_counting = 0
while True:
    action, _states = model.predict(obs, deterministic=True)
    obs, rewards, dones, info = env.step(action)
    env.render()
    if dones:
        obs = env.reset()
        dones_counting += 1
        if dones_counting==3:
            break