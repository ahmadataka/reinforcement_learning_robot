import gym
import numpy as np
import pybulletgym  # register PyBullet enviroments with open ai gym
import matplotlib.pyplot as plt

from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from time import sleep

ENV_ID = "ReacherPyBulletEnv-v0"
env = gym.make(ENV_ID)
env.render()

# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

model = TD3("MlpPolicy", env, action_noise=action_noise, verbose=1)
model.learn(total_timesteps=100000, log_interval=10)
model.save("td3_reacher")
print("model saved, learning completed")

del model # remove to demonstrate saving and loading

model = TD3.load("td3_reacher")

obs = env.reset()
i = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    sleep(0.01)
    # print(i)
    i += 1
    if dones:
        i = 0
        env.reset()
        # print("dones")