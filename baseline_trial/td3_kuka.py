import os
import gym
import numpy as np
import matplotlib.pyplot as plt
import pybullet_envs.bullet.kukaGymEnv as e

from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from time import sleep

env = e.KukaGymEnv(renders=False)
print(env.reset())

# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

model = TD3("MlpPolicy", env, action_noise=action_noise, verbose=1)
model.learn(total_timesteps=1000000, log_interval=10)
model.save("td3_kuka")
print("model saved, learning completed")
env.close()
del model # remove to demonstrate saving and loading
del env

model = TD3.load("td3_kuka")
env = e.KukaGymEnv(renders=False)
obs = env.reset()
i = 0
total_reward = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    # sleep(0.01)
    # print(i)
    i += 1
    total_reward = total_reward + rewards
    if dones:
        i = 0
        print(total_reward)
        total_reward = 0
        env.reset()
        # print("dones")