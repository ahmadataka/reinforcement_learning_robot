import gym
from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy

env = gym.make('Pendulum-v0')

model = PPO('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=200000)
model.save("ppo_pendulum")
del model  # delete trained model to demonstrate loading

# Load the trained agent
model = PPO.load("ppo_pendulum", env=env)

mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

obs = env.reset()
for i in range(1000):
    action, _state = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    # print(i)
    if done:
      obs = env.reset()