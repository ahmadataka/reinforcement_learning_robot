import gym
from stable_baselines3 import PPO
import matplotlib.pyplot as plt
from stable_baselines3.common.evaluation import evaluate_policy

from stable_baselines3.common.env_checker import check_env

from planar_2_link_arm_IK import Planar2LinkArmIK

env = Planar2LinkArmIK()
# It will check your custom environment and output additional warnings if needed
check_env(env)

model = PPO('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=200000)
model.save("ppo_planararmIK_randomgoal")
del model  # delete trained model to demonstrate loading

# Load the trained agent
model = PPO.load("ppo_planararmIK_randomgoal", env=env)

mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

obs = env.reset()
dones_counting = 0
total_reward = 0
error_x = []
error_y = []
speed_x = []
speed_y = []
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    # error_x.append(obs.item(8))
    # speed_x.append(obs.item(6))
    # error_y.append(obs.item(9))
    # speed_y.append(obs.item(7))
    error_x.append(obs.item(0))
    speed_x.append(obs.item(1))
    error_y.append(obs.item(2))
    speed_y.append(obs.item(3))
    if dones:
        obs = env.reset()
        print(total_reward)
        total_reward = 0
        dones_counting += 1
        if dones_counting==10:
            break

fig, ax = plt.subplots()
ax.plot(error_x, '-r', label ='Error x')
ax.plot(error_y, '-g', label ='Error y')
ax.plot(speed_x, '-b', label ='Velocity x')
ax.plot(speed_y, '-k', label ='Velocity y')
plt.xlabel('Step',fontsize=12)
plt.legend()
# plt.xlim(0, 100)
plt.show()
