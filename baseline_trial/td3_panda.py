import os
import gym
import gym_panda
import numpy as np
import matplotlib.pyplot as plt

from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise
from time import sleep
from stable_baselines3.common.env_checker import check_env

# env = gym.make('panda-v0')
from Panda_modif import PandaEnv

env = PandaEnv()

check_env(env)
env.reset()
# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

model = TD3("MlpPolicy", env, action_noise=action_noise, verbose=1)
model.learn(total_timesteps=1000000, log_interval=10)
model.save("td3_panda_reacher_speed")
print("model saved, learning completed")
env.close()
del model # remove to demonstrate saving and loading
del env
model = TD3.load("td3_panda_reacher_speed")
env = PandaEnv(renders=True)
obs = env.reset()
i = 0
total_reward = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    # sleep(0.01)
    # print(i)
    i += 1
    total_reward = total_reward + rewards
    if dones:
        i = 0
        env.reset()
        print(total_reward)
        total_reward = 0
        # print("dones")