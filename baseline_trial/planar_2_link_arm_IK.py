import gym
from gym import spaces
import math
import numpy as np
import random
from gym.utils import seeding

import pybullet as p
import pybullet_data
import time

class Planar2LinkArmIK(gym.Env):
    def __init__(self):
        super(Planar2LinkArmIK, self).__init__()
        self.MAX_EPISODE = 100
        self.delta_t = 0.1
        self.x_threshold = 5
        self.link = self.x_threshold/2.0
        high = np.array([self.x_threshold*2, np.finfo(np.float32).max, self.x_threshold*2, np.finfo(np.float32).max, math.pi, math.pi, self.x_threshold*2, self.x_threshold*2],
                        dtype=np.float32)
        # high = np.array([1.0, 1.0, 1.0, 1.0, self.x_threshold, self.x_threshold, np.finfo(np.float32).max, np.finfo(np.float32).max, self.x_threshold*2, self.x_threshold*2],
        #                 dtype=np.float32)

        self.max_speed = 1
        self.action_space = spaces.Box(-self.max_speed, self.max_speed, shape=(2,), dtype=np.float32)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        self.seed()
        self.reset()
        # self.start_bullet()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def start_bullet(self):
        physicsClient = p.connect(p.GUI)#or p.DIRECT for non-graphical version
        p.setAdditionalSearchPath(pybullet_data.getDataPath()) #optionally
        p.setGravity(0,0,-10)

        self.planeId = p.loadURDF("plane.urdf", [0, 0, 0.0])
        startPos = [0.0,0,0.0]
        startOrientation = p.getQuaternionFromEuler([0,0,0])
        self.arm = p.loadURDF("./2link_planar_arm.urdf")
        self.number_of_joints = p.getNumJoints(self.arm)
        self.joint_indexes = [*range(0, self.number_of_joints, 1)]
        self.active_joints = [1, 1, 0]
    
    def step_speed(self, vel_sent):
        speeds = []
        active_joint_number = 0
        for joint_number in range(0, self.number_of_joints):
            if self.active_joints[joint_number] == 1:
                speeds.append(vel_sent[active_joint_number])
                active_joint_number += 1
            else:
                speeds.append(0)
        print(speeds)
        p.setJointMotorControlArray(self.arm, self.joint_indexes,
                                    p.VELOCITY_CONTROL,
                                    targetVelocities=speeds)
        
        p.stepSimulation()

    def forward_kinematics(self, c_sp):
        theta = (c_sp[0], c_sp[2])
        theta_dot = (c_sp[1], c_sp[3])
        x_ee = self.link*(math.cos(theta[0]) + math.cos(theta[0]+theta[1]))
        y_ee = self.link*(math.sin(theta[0]) + math.sin(theta[0]+theta[1]))
        x_ee_dot = self.link*(-theta_dot[0]*math.sin(theta[0]) -theta_dot[0]*math.sin(theta[0]+theta[1])- theta_dot[1]*math.sin(theta[0]+theta[1]))
        y_ee_dot = self.link*(theta_dot[0]*math.cos(theta[0]) + theta_dot[0]*math.cos(theta[0]+theta[1]) + theta_dot[1]*math.cos(theta[0]+theta[1]))
        
        return (x_ee, x_ee_dot, y_ee, y_ee_dot)

    def step(self, action):
        self.cur_state = self.state
        save_state = self.state
        action = np.clip(action, -self.max_speed, self.max_speed)
        self.act = action
        # self.step_speed(action)

        for i in range(0, 2):
            self.angle[i] = self.angle[i] + action[i]*self.delta_t
            if(self.angle[i]>math.pi):
                self.angle[i] = -(2*math.pi - self.angle[i])
            elif(self.angle[i]<-math.pi):
                self.angle[i] = (2*math.pi - abs(self.angle[i]))
            self.angle_dot[i] = action[i]

        self.c_space = (self.angle[0], self.angle_dot[0], self.angle[1], self.angle_dot[1])
        self.x_space = self.forward_kinematics(self.c_space)
        self.state = (self.x_space[0]-self.x_target[0], self.x_space[1], self.x_space[2]-self.x_target[1], self.x_space[3], self.angle[0], self.angle[1], self.x_target[0], self.x_target[1])
        # self.state = (math.cos(self.angle[0]), math.cos(self.angle[1]), math.sin(self.angle[0]), math.sin(self.angle[1]), self.x_target[0], self.x_target[1], self.x_space[1], self.x_space[3], self.x_space[0]-self.x_target[0], self.x_space[2]-self.x_target[1])

        done = bool(
            self.steps_left<0
        )
        if not done:
            reward = - (save_state[0]**2+save_state[2]**2) - 0.5*(action[0]**2+action[1]**2)
            # reward = - x**2
        else:
            reward = 0
        if not done:
            self.steps_left = self.steps_left-1
        self.cur_done = done
        return np.array(self.state), reward, done, {}

    def reset(self):
        # Produce random target
        radius_target = self.np_random.uniform(low=0.5*self.link, high=2*self.link, size=(1,))
        angle_target = self.np_random.uniform(low=-math.pi, high=math.pi, size=(1,))
        self.x_target = (radius_target[0]*math.cos(angle_target[0]),radius_target[0]*math.sin(angle_target[0]))
        # self.x_target = (3.0,3.0)
        self.steps_left = self.MAX_EPISODE
        self.angle = list(self.np_random.uniform(low=-math.pi, high=math.pi, size=(2,)))
        self.angle_dot = list((0.0,0.0))
        self.c_space = (self.angle[0], self.angle_dot[0], self.angle[1], self.angle_dot[1])
        self.x_space = self.forward_kinematics(self.c_space)
        self.state = (self.x_space[0]-self.x_target[0], self.x_space[1], self.x_space[2]-self.x_target[1], self.x_space[3], self.angle[0], self.angle[1], self.x_target[0], self.x_target[1])
        # self.state = (math.cos(self.angle[0]), math.cos(self.angle[1]), math.sin(self.angle[0]), math.sin(self.angle[1]), self.x_target[0], self.x_target[1], self.x_space[1], self.x_space[3], self.x_space[0]-self.x_target[0], self.x_space[2]-self.x_target[1])
        return np.array(self.state)

    def render(self, mode='human'):
        print(f'State {self.cur_state}, action: {self.act}, done: {self.cur_done}')
        print(f'C Space {self.c_space}, X Space: {self.x_space}, goal: {self.x_target}')