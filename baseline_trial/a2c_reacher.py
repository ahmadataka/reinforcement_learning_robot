import gym
import numpy as np
import pybulletgym  # register PyBullet enviroments with open ai gym

from stable_baselines3 import A2C

from time import sleep

ENV_ID = "ReacherPyBulletEnv-v0"
env = gym.make(ENV_ID)
env.render()

model = A2C('MlpPolicy', env, verbose=1, device="cpu")
model.learn(total_timesteps=100000, log_interval=10)
model.save("a2c_reacher")
env = model.get_env()
print("learning finished, model saved")
del model # remove to demonstrate saving and loading

model = A2C.load("a2c_reacher", device="cpu")

obs = env.reset()
i = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    sleep(0.01)
    # print(i)
    i += 1
    if dones:
        i = 0
        env.reset()
        # print("dones")