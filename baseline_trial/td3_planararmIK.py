import gym
import numpy as np
import matplotlib.pyplot as plt

from stable_baselines3 import TD3
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise

from stable_baselines3.common.env_checker import check_env

from planar_2_link_arm_IK import Planar2LinkArmIK

env = Planar2LinkArmIK()
# It will check your custom environment and output additional warnings if needed
check_env(env)
print(env.reset())
print(env.observation_space)
# The noise objects for TD3
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

model = TD3("MlpPolicy", env, action_noise=action_noise, verbose=1)
model.learn(total_timesteps=100000, log_interval=10)
model.save("td3_planararmIK_randomgoal")
env = model.get_env()

del model # remove to demonstrate saving and loading

model = TD3.load("td3_planararmIK_randomgoal")

obs = env.reset()
dones_counting = 0
total_reward = 0
error_x = []
error_y = []
speed_x = []
speed_y = []
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    # error_x.append(obs.item(8))
    # speed_x.append(obs.item(6))
    # error_y.append(obs.item(9))
    # speed_y.append(obs.item(7))
    error_x.append(obs.item(0))
    speed_x.append(obs.item(1))
    error_y.append(obs.item(2))
    speed_y.append(obs.item(3))
    if dones:
        obs = env.reset()
        print(total_reward)
        total_reward = 0
        dones_counting += 1
        if dones_counting==10:
            break

fig, ax = plt.subplots()
ax.plot(error_x, '-r', label ='Error x')
ax.plot(error_y, '-g', label ='Error y')
ax.plot(speed_x, '-b', label ='Velocity x')
ax.plot(speed_y, '-k', label ='Velocity y')
plt.xlabel('Step',fontsize=12)
plt.legend()
# plt.xlim(0, 100)
plt.show()
