import gym
from stable_baselines3 import A2C
from stable_baselines3.common.evaluation import evaluate_policy

from stable_baselines3.common.env_checker import check_env

from SingleIntegratorV1 import SingleIntegrator

env = SingleIntegrator()
# It will check your custom environment and output additional warnings if needed
check_env(env)

model = A2C('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=20000)
model.save("a2c_singleintdisc")
del model  # delete trained model to demonstrate loading

# Load the trained agent
model = A2C.load("a2c_singleintdisc", env=env)

mean_reward, std_reward = evaluate_policy(model, model.get_env(), n_eval_episodes=10)

obs = env.reset()
for i in range(500):
    action, _state = model.predict(obs, deterministic=True)
    obs, reward, done, info = env.step(action)
    env.render()
    # print(i)
    if done:
      obs = env.reset()