import os
import gym
import numpy as np
import matplotlib.pyplot as plt
import pybullet_envs.bullet.kukaGymEnv as e

from stable_baselines3 import SAC
from time import sleep

env = e.KukaGymEnv(renders=False)
print(env.reset())
print(env.observation_space)

model = SAC('MlpPolicy', env, verbose=1)
model.learn(total_timesteps=1000000)
model.save("sac_kuka")
print("model saved, learning completed")

del model # remove to demonstrate saving and loading

model = SAC.load("sac_kuka")

env = e.KukaGymEnv(renders=True)
obs = env.reset()
i = 0
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    # sleep(0.01)
    print(i)
    i += 1
    if dones:
        i = 0
        env.reset()
        # print("dones")