import gym
import numpy as np
import pybullet_envs

from stable_baselines3 import DDPG
from stable_baselines3.common.noise import NormalActionNoise, OrnsteinUhlenbeckActionNoise

# ENV_ID = "HopperBulletEnv-v0"
ENV_ID = "MinitaurBulletEnv-v0"
spec = gym.envs.registry.spec(ENV_ID)
spec._kwargs['render'] = True
env = gym.make(ENV_ID)
env.reset()
# The noise objects for DDPG
n_actions = env.action_space.shape[-1]
action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.1 * np.ones(n_actions))

model = DDPG("MlpPolicy", env, action_noise=action_noise, verbose=1)
model.learn(total_timesteps=10000, log_interval=10)
model.save("ddpg_hopper")
env = model.get_env()

del model # remove to demonstrate saving and loading

model = DDPG.load("ddpg_hopper")

obs = env.reset()
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()