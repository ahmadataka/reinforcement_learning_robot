import gym
from gym import spaces
import math
import numpy as np
import random

class SingleIntegratorCont(gym.Env):
    def __init__(self):
        super(SingleIntegratorCont, self).__init__()
        self.MAX_EPISODE = 100
        self.x_threshold = 5
        self.delta_t = 0.1
        high = np.array([self.x_threshold],
                        dtype=np.float32)

        self.max_speed = 1
        self.action_space = spaces.Box(-self.max_speed, self.max_speed, shape=(1,), dtype=np.float32)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        self.steps_left = self.MAX_EPISODE
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.x_target = 0

    def step(self, action):
        self.cur_state = self.state
        x = self.state
        save_state = self.state
        action = np.clip(action, -self.max_speed, self.max_speed)[0]
        self.act = action
        x = x + action*self.delta_t
        self.state = x
        done = bool(
            x < -self.x_threshold
            or x > self.x_threshold
            or self.steps_left<0
        )
        if not done:
            reward = - save_state**2 - 0.05*action**2
            # reward = - x**2
        else:
            if x< - self.x_threshold or x > self.x_threshold:
                reward = -x**2*self.MAX_EPISODE
            else:
                reward = 0
        # if reward< -100:
        #     reward = -100
        if not done:
            self.steps_left = self.steps_left-1
        self.cur_done = done
        return np.array([self.state]), reward, done, {}

    def reset(self):
        self.state = random.randint(-self.x_threshold, self.x_threshold)
        self.steps_left = self.MAX_EPISODE
        return np.array([self.state])

    def render(self, mode='human'):
        print(f'State {self.cur_state}, action: {self.act}, done: {self.cur_done}')