import pybullet as p
import time
import pybullet_data
import math
physicsClient = p.connect(p.GUI)#or p.DIRECT for non-graphical version
p.setAdditionalSearchPath(pybullet_data.getDataPath()) #optionally
p.setGravity(0,0,-10)

planeId = p.loadURDF("plane.urdf")

startPos = [0,0,0.0]
startOrientation = p.getQuaternionFromEuler([0,0,0])
boxId = p.loadURDF("franka_panda/panda.urdf",startPos, startOrientation)
EndEffectorIndex = 6

constId = p.createConstraint(planeId, -1, boxId, -1, p.JOINT_FIXED, [0, 0, 0], [0, 0, 0], [0., 0., 0.],
                         [0, 0, 0, 1])
#set the center of mass frame (loadURDF sets base link frame) startPos/Ornp.resetBasePositionAndOrientation(boxId, startPos, startOrientation)
number_of_joints = p.getNumJoints(boxId)
control_bar = []
for joint_number in range(0, number_of_joints):
    info = p.getJointInfo(boxId, joint_number)
    print(info[0], ": ", info[1])

    control_bar.append(p.addUserDebugParameter(str(info[1]), -0.5, 0.5, 0))

joint_indexes = [*range(0, number_of_joints, 1)]

# Get the EE position
current_ee_pose = p.getLinkState(boxId,EndEffectorIndex)
current_ee_position = current_ee_pose[4]
print(current_ee_position)
# ee_target = (current_ee_position[0]+0.1, current_ee_position[1]+0.1, current_ee_position[2]-0.1)
ee_target = (0.07570351660251617, -0.03769034519791603, 0.9671298861503601)
ee_z_ori = ee_target[2]
ik_active = 1
time_ = 0
while True:
    # for link_number in range(0, number_of_joints):
    #     print("link"+str(link_number))
    #     print(p.getLinkState(boxId,link_number))
    # p.setJointMotorControl2(boxId, 0,
    #                             p.POSITION_CONTROL,
    #                             targetPosition=angle)
    if ik_active == False:
        angle = []
        for joint_number in range(0, number_of_joints):
            angle.append(p.readUserDebugParameter(control_bar[joint_number]))
    else:
        angle = p.calculateInverseKinematics(boxId,
                                            EndEffectorIndex,
                                            ee_target,
                                            solver=p.IK_DLS)
        angle = list(angle)
        while(len(angle)<number_of_joints):
            angle.append(0.0)
    
    # print(angle)
    p.setJointMotorControlArray(boxId, joint_indexes,
                                p.POSITION_CONTROL,
                                targetPositions=angle)
    
    current_ee_pose = p.getLinkState(boxId,EndEffectorIndex)
    current_ee_position = current_ee_pose[4]
    print("compare")
    print(current_ee_pose)
    print(ee_target)
    ee_target_list = list(ee_target)
    ee_target_list[2] = ee_z_ori + 0.1*math.cos(2*math.pi*time_/5.0)
    ee_target = tuple(ee_target_list)
    p.stepSimulation()
    time.sleep(1./240.)
    time_ = time_ + 1./240.