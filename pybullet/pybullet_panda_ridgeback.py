import pybullet as p
import time
import pybullet_data
import math
physicsClient = p.connect(p.GUI)#or p.DIRECT for non-graphical version
p.setAdditionalSearchPath(pybullet_data.getDataPath()) #optionally
p.setGravity(0,0,-10)

planeId = p.loadURDF("plane.urdf", [0, 0, 0.0])
startPos = [0.0,0,0.0]
startOrientation = p.getQuaternionFromEuler([0,0,0])
ridgeback = p.loadURDF("urdf/ridgeback.urdf")
startPos = [0.1,0,0.3]
startOrientation = p.getQuaternionFromEuler([0,0,0])
boxId = p.loadURDF("franka_panda/panda.urdf",startPos, startOrientation)
EndEffectorIndex = 6

constId = p.createConstraint(ridgeback, -1, boxId, -1, p.JOINT_FIXED, [0, 0, 0], startPos, [0., 0., 0.],
                         [0, 0, 0, 1])
#set the center of mass frame (loadURDF sets base link frame) startPos/Ornp.resetBasePositionAndOrientation(boxId, startPos, startOrientation)
number_of_joints = p.getNumJoints(boxId)
active_joints = [1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0]
lower_limit = [-2.9671, -1.8326, -2.9671, -3.1416, -2.9671, -0.0873, -2.9671, 0, 0, 0.0, 0.0, 0]
upper_limit = [2.9671, 1.8326, 2.9671, 0.0, 2.9671, 3.8223, 2.9671, 0, 0, 0.04, 0.04, 0]
control_bar = []
for joint_number in range(0, number_of_joints):
    if active_joints[joint_number] == 1:
        info = p.getJointInfo(boxId, joint_number)
        print(info[0], ": ", info[1])

        control_bar.append(p.addUserDebugParameter(str(info[1]), lower_limit[joint_number], upper_limit[joint_number], 0))

joint_indexes = [*range(0, number_of_joints, 1)]

number_of_joints_rb = p.getNumJoints(ridgeback)
active_joints_rb = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0]
lower_limit_rb = [0, 0, 0, 0, 0, 0, 0, 0, 0, -0.08726,-62.8, -62.8, 0, -62.8, -62.8, 0, 0]
upper_limit_rb = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0.08726, 62.8, 62.8, 0, 62.8, 62.8, 0, 0]
control_bar_rb = []
for joint_number in range(0, number_of_joints_rb):
    if active_joints_rb[joint_number] == 1:
        info = p.getJointInfo(ridgeback, joint_number)
        print(info[0], ": ", info[1])

        control_bar_rb.append(p.addUserDebugParameter(str(info[1]), lower_limit_rb[joint_number], upper_limit_rb[joint_number], 0))
    # control_bar_rb.append(p.addUserDebugParameter(str(info[1]), -0.5, 0.5, 0))
joint_indexes_rb = [*range(0, number_of_joints_rb, 1)]

time_ = 0
while True:
    angle = []
    active_joint_number = 0
    for joint_number in range(0, number_of_joints):
        if active_joints[joint_number] == 1:
            angle.append(p.readUserDebugParameter(control_bar[active_joint_number]))
            active_joint_number += 1
        else:
            angle.append(0)
    
    p.setJointMotorControlArray(boxId, joint_indexes,
                                p.POSITION_CONTROL,
                                targetPositions=angle)
    
    angle = []
    active_joint_number = 0
    for joint_number in range(0, number_of_joints_rb):
        if active_joints_rb[joint_number] == 1:
            angle.append(p.readUserDebugParameter(control_bar_rb[active_joint_number]))
            active_joint_number += 1
        else:
            angle.append(0)
    
    p.setJointMotorControlArray(ridgeback, joint_indexes_rb,
                                p.POSITION_CONTROL,
                                targetPositions=angle)
    
    p.stepSimulation()
    time.sleep(1./240.)
    time_ = time_ + 1./240.