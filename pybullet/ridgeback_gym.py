import pybullet as p
import time
import pybullet_data
import math
physicsClient = p.connect(p.GUI)#or p.DIRECT for non-graphical version
p.setAdditionalSearchPath(pybullet_data.getDataPath()) #optionally
p.setGravity(0,0,-10)

planeId = p.loadURDF("plane.urdf", [0, 0, 0.0])
startPos = [0.0,0,0.0]
startOrientation = p.getQuaternionFromEuler([0,0,0])
ridgeback = p.loadURDF("urdf/ridgeback.urdf")

number_of_joints_rb = p.getNumJoints(ridgeback)
active_joints_rb = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0]
for joint_number in range(0, number_of_joints_rb):
    # if active_joints_rb[joint_number] == 1:
    info = p.getJointInfo(ridgeback, joint_number)
    print(info[0], ": ", info[1])
joint_indexes_rb = [*range(0, number_of_joints_rb, 1)]

time_ = 0
vel_sent = [0.0, 1.0, -1.0, 1.0, -1.0]
vel_mag = 3.0
vel_sent = [x * vel_mag for x in vel_sent]
print(vel_sent)
while True:
    speed = []
    active_joint_number = 0
    
    for joint_number in range(0, number_of_joints_rb):
        if active_joints_rb[joint_number] == 1:
            speed.append(vel_sent[active_joint_number])
            active_joint_number += 1
        else:
            speed.append(0)
    # print(joint_indexes_rb)
    # print(speed)
    p.setJointMotorControlArray(ridgeback, joint_indexes_rb,
                                p.VELOCITY_CONTROL,
                                targetVelocities=speed)
    
    p.stepSimulation()
    time.sleep(1./240.)
    time_ = time_ + 1./240.
p.disconnect()