import gym
import time

if __name__ == "__main__":
    env = gym.make("CartPole-v0")
    # env = gym.wrappers.Monitor(env, "recording")
    total_reward = 0.0
    total_steps = 0
    obs = env.reset()

    while True:
        env.render()
        time.sleep(1)
        action = env.action_space.sample()# random action
        obs, reward, done, _ = env.step(action)
        total_reward += reward
        total_steps += 1
        print("Steps: %d", total_steps)
        print("Reward: %.2f", total_reward)
        print(env.action_space.n)
        print(env.observation_space.shape[0])
        if done:
            break
    
    print("Episode done in %d steps, total reward %.2f" % (total_steps, total_reward))